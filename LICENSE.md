License
=======

Cette œuvre est mise à disposition sous licence Attribution -  Partage dans les Mêmes Conditions 3.0 France.

Pour voir une copie de cette licence, visitez http://creativecommons.org/licenses/by-sa/3.0/fr/ ou écrivez à Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

---

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.

To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.