TP Routage
============

*Eliott Goyé - LP ASUR Groupe 2*

# Schéma du réseau

![schema](https://lh3.googleusercontent.com/-SpzvzmTA4qI/V9_n3N6g1MI/AAAAAAAACm0/r2eGVPJnlBMpeW03rRuQFidwvMqxjLdHACLcB/s0/R%25C3%25A9seau+routage.png "Réseau routage.png")

# Mise en place des liens

Éditer le fichier `lab.conf`

La syntaxe est la suivante :

`nom_machine[interface]=domaine_de collision`

```
pc01[0]=h1
pc02[0]=h2
pc03[0]=h3

r1[0]=h1
r2[0]=h2
r3[0]=h3

r1[1]=h4
r1[2]=h5

r2[1]=h5
r2[2]=h6


r3[1]=h4
r3[2]=h6
```

#Configuration IP des machines

## PCs

`pc01.startup`

```
ifconfig eth0 192.168.1.1 netmask 255.255.255.0

route add default gw 192.168.1.254 dev eth0
```
---
`pc02.startup`

```
ifconfig eth0 192.168.2.1 netmask 255.255.255.0

route add default gw 192.168.2.254 dev eth0
```
---
`pc03.startup`

```
ifconfig eth0 192.168.3.1 netmask 255.255.255.0

route add default gw 192.168.3.254 dev eth0
```

## Routeurs

`r1.startup`

```
ifconfig eth0 192.168.1.254 netmask 255.255.255.0
ifconfig eth1 192.168.4.1 netmask 255.255.255.0
ifconfig eth2 192.168.5.1 netmask 255.255.255.0

route add -net 192.168.2.0 netmask 255.255.255.0 gw 192.168.5.2
route add -net 192.168.3.0 netmask 255.255.255.0 gw 192.168.4.2
```
---
`r2.startup`

```
ifconfig eth0 192.168.2.254 netmask 255.255.255.0
ifconfig eth1 192.168.5.2 netmask 255.255.255.0
ifconfig eth2 192.168.6.2 netmask 255.255.255.0

route add -net 192.168.1.0 netmask 255.255.255.0 gw 192.168.5.1
route add -net 192.168.3.0 netmask 255.255.255.0 gw 192.168.6.1
```
---
`r3.startup`

```
ifconfig eth0 192.168.3.254 netmask 255.255.255.0
ifconfig eth1 192.168.4.2 netmask 255.255.255.0
ifconfig eth2 192.168.6.1 netmask 255.255.255.0

route add -net 192.168.1.0 netmask 255.255.255.0 gw 192.168.4.1
route add -net 192.168.2.0 netmask 255.255.255.0 gw 192.168.6.2
```

Lancer le labo :
`lstart -f`

Les machines peuvent se pinger à présent.

